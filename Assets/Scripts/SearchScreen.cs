﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Models;
using TMPro;
using UniRx;
using UnityEngine;
using Zenject;

public class SearchScreen : MonoBehaviour
{
    [Inject] DiContainer _container;
    [SerializeField] private MovieObject movieObject;
    [SerializeField] private Transform content;
    [SerializeField] private TMP_InputField searchInput;
    [SerializeField] private TextMeshProUGUI infoTxt;
    
    private ImdbApiService imdbApiService = new ImdbApiService();
    private List<MovieInfo> movies = new List<MovieInfo>();
    private List<MovieObject> movieObjects = new List<MovieObject>();
    private CancellationTokenSource cancelToken;

    public void SetInteractable(bool value)
    {
        GetComponent<CanvasGroup>().interactable = value;
    }

    private void Start()
    {
        Debug.Log(_container);
        searchInput.onValueChanged.AsObservable().Subscribe(async s =>
        {
            if (s.Length >= 3)
            {
                cancelToken?.Cancel();
                cancelToken?.Dispose();
                cancelToken = new CancellationTokenSource();
                await ShowMovies(s, cancelToken.Token);
            }
            else
            {
                cancelToken?.Cancel();
                Clear();
                HideInfo();
            }

        }).AddTo(searchInput);
    }

    private async Task ShowMovies(string s,CancellationToken token)
    {
        if (token.IsCancellationRequested)
            return;
        Clear();
        ShowInfo("Loading...");
        var imdbSearch = await imdbApiService.GetMoviesBySearchString(s);
  
        if (imdbSearch.Response != "False")
        {
            for (int i = 0; i < imdbSearch.Search.Count; i++)
            {
                if (token.IsCancellationRequested)
                {
                    Clear();
                    return;
                }
                var movieInfo = await imdbApiService.GetMovieById(imdbSearch.Search[i].ImdbId);
                movies.Add(movieInfo);
            }

            movies.Sort(new RatingComparer());
            HideInfo();
            InstantiateMovies();
        }
        else
        {
            ShowInfo("Movie not found!");
        }
    }

    private void InstantiateMovies()
    {
        movies.ForEach(movie =>
        {
            var go = _container.InstantiatePrefab(movieObject,content);
            var movieGo = go.GetComponent<MovieObject>();
            _container.Inject(go);
            movieGo.Init(movie);
            movieObjects.Add(movieGo);
        });
    }

    private void ShowInfo(string info)
    {
        infoTxt.text = info;
        infoTxt.gameObject.SetActive(true);
    }
    
    private void HideInfo()
    {
        infoTxt.gameObject.SetActive(false);
    }

    private void Clear()
    {
        movies.Clear();
        movieObjects.ForEach(_=>Destroy(_.gameObject));
        movieObjects.Clear();
    }

}

class RatingComparer : IComparer<MovieInfo> 
{ 
    public int Compare(MovieInfo x, MovieInfo y)
    {
        decimal x1;
        decimal x2;
        try
        {
            x1 = Convert.ToDecimal(x.ImdbRating, new CultureInfo("en-US"));
            x2 = Convert.ToDecimal(y.ImdbRating, new CultureInfo("en-US"));
        }
        catch (Exception e)
        {
            return 0;
        }
        
       if(x1 == 0 || x2 == 0 )
        return 0;
       return x2.CompareTo(x1);
    } 
} 
