﻿using System;
using DG.Tweening;
using Models;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class DetailScreen : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI ratingTxt,yearTxt,timeTxt;
    [SerializeField] private TextMeshProUGUI actorsTxt,descriptionTxt;
    [SerializeField] private RawImage poster;
    [SerializeField] private Button backBtn;
    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private Texture2D defaultPoster;
    [Inject] private SearchScreen searchScreen;
    private RectTransform rootCanvasRect,myRect;
    [SerializeField] private VerticalLayoutGroup verticalLayout;
    
    private void Awake()
    {
        rootCanvasRect = transform.root.GetComponent<RectTransform>();
        myRect = transform.GetComponent<RectTransform>();
    }

    private void Start()
    {
        Vector3 newPosition = rootCanvasRect.anchoredPosition;
        newPosition.x = +Screen.width;
        myRect.anchoredPosition = newPosition;
        gameObject.SetActive(false);
        backBtn.OnClickAsObservable()
            .Subscribe(_ => Hide());
    }

    
    public void Show(MovieInfo movieInfo,Texture2D posterImage)
    {
        SetInfo(movieInfo,posterImage);
        gameObject.SetActive(true);
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)verticalLayout.transform );
        scrollRect.normalizedPosition = new Vector2(0,1);
        GetComponent<CanvasGroup>().interactable = true;
        searchScreen.SetInteractable(false);
        myRect.DOAnchorPos(rootCanvasRect.anchoredPosition, 0.5f);
    }
    
    private void SetInfo(MovieInfo movieInfo,Texture posterImage)
    {
        title.text = movieInfo.Title;
        ratingTxt.text = "IMDb: " + movieInfo.ImdbRating;
        yearTxt.text = movieInfo.Year;
        timeTxt.text = movieInfo.Runtime;
        actorsTxt.text = "<b>Actors:</b> \n" +
                         movieInfo.Actors;
        descriptionTxt.text = "<b>Description:</b> \n" +
                              movieInfo.Plot;
        poster.texture = posterImage!=null ? posterImage : defaultPoster;
    }


    private void Hide()
    {
        Vector3 newPosition = rootCanvasRect.anchoredPosition;
        newPosition.x = +rootCanvasRect.sizeDelta.x;
        GetComponent<CanvasGroup>().interactable = false;
        myRect.DOAnchorPos(newPosition, 0.5f).OnComplete(()=>
        {
            gameObject.SetActive(false);
            searchScreen.SetInteractable(true);
        });
    }
}
