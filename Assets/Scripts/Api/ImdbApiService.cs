﻿using System.Threading.Tasks;
using ApiService;
using ApiService.Json;
using Models;
using UnityEngine;

public class ImdbApiService
{
    private string url = "http://www.omdbapi.com/?apikey=b481085f";
    
    public async Task<ImdbSearch> GetMoviesBySearchString(string name)
    {
        var json = await ApiRequest.GetRequest($"{url}&s={name}&type=movie");
        var result = await JsonConverterImpl.FromJsonAsync<ImdbSearch>(json);
       return result;
    }

    public async Task<MovieInfo> GetMovieById(string imbdId)
    {
        var json = await ApiRequest.GetRequest($"{url}&i={imbdId}&plot=full");
        if (!string.IsNullOrEmpty(json)) return await JsonConverterImpl.FromJsonAsync<MovieInfo>(json);
        Debug.LogError("Wrong id");
        return null;
    }
}
