﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using System;
using System.Threading.Tasks;

namespace ApiService.Json
{
    public class JsonConverterImpl
    {

        private static readonly List<string> errors = new List<string>();

        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Include,
            Error = delegate(object sender, Newtonsoft.Json.Serialization.ErrorEventArgs earg)
            {
                errors.Add(earg.ErrorContext.Member.ToString());
                earg.ErrorContext.Handled = true;
            },
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                ParseStringConverter.Singleton,
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            }
        };

        public static T FromJson<T>(string json)
        {
            Debug.Log("Json : " + json);
            T x = JsonConvert.DeserializeObject<T>(json, Settings);
            errors.ForEach(e => Debug.LogError("Parser Error " + e));
            return x;
        }
        
        public static async Task<T> FromJsonAsync<T>(string json)
        {
            await new WaitForBackgroundThread();
            Debug.Log("Json : " + json);
            T x = JsonConvert.DeserializeObject<T>(json, Settings);
            errors.ForEach(e => Debug.LogError("Parser Error " + e));
            return x;
        }

        public static string ToJson(object obj)
        {
            string json = JsonConvert.SerializeObject(obj, Settings);
            Debug.Log("Json : " + json);
            return json;
        }
    }
    
    class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}
