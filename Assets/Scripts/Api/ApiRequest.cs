﻿using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace ApiService
{
    public static class ApiRequest
    {
        #region Get

        public static async Task<string> GetRequest(string url)
        {
            UnityWebRequest request = UnityWebRequest.Get(url);
            await request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError(request.error);
                return null;
            }

            return request.downloadHandler.text;
        }
        
        public static async Task<string> HttpWebGetRequest(string url)
        {
            string json = string.Empty;
            await new WaitForBackgroundThread();
            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                json = reader.ReadToEnd();
            }
            await new WaitForUpdate();
            return json;
        }

        #endregion

        #region Post       

        public static async Task<string> PostRequest(string url)
        {
            UnityWebRequest request = UnityWebRequest.Post(url, "");

            request.SetRequestHeader("Content-Type", "application/json");
//            if (!string.IsNullOrEmpty(AuthToken))
//            {
////                request.SetRequestHeader("X-CSRFToken",AuthToken);
//                request.SetRequestHeader("Authorization", "Bearer " + AuthToken);
//            }

            await request.SendWebRequest();
            return request.downloadHandler.text;
        }

        public static async Task<string> PostRequest(string url, string dataObj)
        {
            var bodyRaw = new System.Text.UTF8Encoding().GetBytes(dataObj);
            UnityWebRequest request = new UnityWebRequest(url, "POST")
            {
                uploadHandler = new UploadHandlerRaw(bodyRaw), downloadHandler = new DownloadHandlerBuffer()
            };

//            request.SetRequestHeader("Content-Type", "application/json");
//            if (!string.IsNullOrEmpty(AuthToken))
//            {
//                request.SetRequestHeader("Authorization", "Bearer " + AuthToken);
//            }

            await request.SendWebRequest();
            return request.downloadHandler.text;
        }

        #endregion

        #region Patch

        public static async Task<string> Patch(string url, string dataObj)
        {
            var bodyRaw = new System.Text.UTF8Encoding().GetBytes(dataObj);

            UnityWebRequest request = new UnityWebRequest(url, "PATCH")
            {
                uploadHandler = new UploadHandlerRaw(bodyRaw), downloadHandler = new DownloadHandlerBuffer()
            };

            request.SetRequestHeader("Content-Type", "application/json");
//            if (!string.IsNullOrEmpty(AuthToken))
//                request.SetRequestHeader("X-CSRFToken", AuthToken);

            await request.SendWebRequest();

            return request.downloadHandler.text;
        }

        #endregion

        #region GetImages
        
        public static async Task<Texture2D> GetImageAsync(string url)
        {
            if (string.IsNullOrEmpty(url)) return null;
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
            await request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
                return null;
            return DownloadHandlerTexture.GetContent( request );
        }

        public static IEnumerator GetImage(string url,Action<Texture2D> onComplete = null,Action onError = null)
        {
            if (string.IsNullOrEmpty(url)) yield break;
            if (url.Equals("N/A"))
            {
                onError?.Invoke();
                yield break;
            }

            UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                onError?.Invoke();
                yield break;
            }

            onComplete?.Invoke(DownloadHandlerTexture.GetContent(request));
//            img.texture = DownloadHandlerTexture.GetContent(request);
        }
        
        #endregion
    }
}