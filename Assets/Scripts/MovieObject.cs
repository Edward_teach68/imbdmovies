﻿using ApiService;
using DG.Tweening;
using Models;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MovieObject : MonoBehaviour
{
    
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI director;
    [SerializeField] private TextMeshProUGUI year;
    [SerializeField] private TextMeshProUGUI rating;
    [SerializeField] private RawImage poster;
    [SerializeField] private Button clickBtn;
    private Coroutine imageCoroutine;
    private DetailScreen detailScreen;
    private Texture2D posterImage;

    [Inject]
    public void Construct(DetailScreen _detailScreen)
    {
        detailScreen = _detailScreen;
    }
  
    public void Init(MovieInfo movieInfo)
    {
        title.text = movieInfo.Title;
        director.text = "Director: " + movieInfo.Director;
        year.text = "Year: " + movieInfo.Year;
        rating.text = "IMDb " + movieInfo.ImdbRating;
        imageCoroutine =
            StartCoroutine(ApiRequest.GetImage(movieInfo.Poster.ToString(),
                image =>
                {
                    posterImage = image;
                    poster.texture = image;
                    Show();
                },
                Show));
        
        clickBtn.OnClickAsObservable()
            .Subscribe(_ =>detailScreen.Show(movieInfo,posterImage)).AddTo(this);
    }


    private void Show()
    {
        GetComponent<CanvasGroup>().DOFade(1, 0.5f);
    }

    private void OnDestroy()
    {
        if(imageCoroutine!=null)
            StopCoroutine(imageCoroutine);
    }
}