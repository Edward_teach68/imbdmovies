﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EntryPoint : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<SearchScreen>().FromComponentInHierarchy().AsSingle();
        Container.Bind<DetailScreen>().FromComponentInHierarchy().AsSingle();
    }

    public override void Start()
    {
#if UNITY_IOS
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
#else
        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = -1;
#endif
    }
}
