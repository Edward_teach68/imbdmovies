﻿using System.Collections.Generic;
using ApiService.Json;
using Newtonsoft.Json;
using UnityEngine;

namespace Models
{
    public class ImdbSearch : MonoBehaviour
    {
        [JsonProperty("Search")]
        public List<MovieSearch> Search { get; set; }

        [JsonProperty("totalResults")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long TotalResults { get; set; }

        [JsonProperty("Response")]
        public string Response { get; set; }
    }
}
