﻿using System;
using ApiService.Json;
using Newtonsoft.Json;
using UnityEngine;

namespace Models
{
    public class MovieSearch
    {
        [JsonProperty("imdbID")]
        public string ImdbId { get; set; }
    }
    
    public class MovieInfo
    {
        [JsonProperty("Title")]
        public string Title { get; set; }
        
        [JsonProperty("Director")]
        public string Director { get; set; }
        
        [JsonProperty("Plot")]
        public string Plot { get; set; }

        [JsonProperty("Year")]
        public string Year { get; set; }
        
        [JsonProperty("imdbRating")]
        public string ImdbRating { get; set; }
        
        [JsonProperty("Runtime")]
        public string Runtime { get; set; }
        
        [JsonProperty("Actors")]
        public string Actors { get; set; }


        [JsonProperty("imdbID")]
        public string ImdbId { get; set; }

        [JsonProperty("Poster")]
        public Uri Poster { get; set; }

        public Texture2D PosterImg { get; set; }
    }
}
