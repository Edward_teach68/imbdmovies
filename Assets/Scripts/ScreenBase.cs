﻿using UnityEngine;

public class ScreenBase : MonoBehaviour
{
    public virtual void Show()
    {
        
    }

    public virtual void Hide()
    {
       
    }
}
